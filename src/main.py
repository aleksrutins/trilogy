import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
gi.require_version('Kaste', '0.1')
gi.require_version('Tlg', '0.1')

from gi.repository import Gtk, Gdk, Gio, Adw
from .window import TrilogyWindow


class TrilogyApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id='com.rutins.Trilogy',
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        self.create_action('quit', lambda *_: self.quit(), ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)

    def do_activate(self):
        """Called when the application is activated.

        We create the style manager, and raise the application's main window, creating it if
        necessary.
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_resource('/com/rutins/Trilogy/app.css')
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), style_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
        win = self.props.active_window
        if not win:
            win = TrilogyWindow(application=self)
        win.present()

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='Trilogy',
                                application_icon='com.rutins.Trilogy',
                                developer_name='Aleks Rutins',
                                version='0.1.0',
                                developers=['Aleks Rutins'],
                                copyright='© 2023 Aleks Rutins')
        about.present()

    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        print('app.preferences action activated')

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    app = TrilogyApplication()
    return app.run(sys.argv)
